package mx.unitec.moviles.practica5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import mx.unitec.moviles.practica5.service.MyjobIntenService

class jobActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_job)
        val intent= Intent(this, MyjobIntenService::class.java)
        intent.putExtra("max",100)
        MyjobIntenService.enqueueWork(this,intent)

    }
}